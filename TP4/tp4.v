Parameter E : Type.
Parameter R : E -> E -> Prop.
Axiom refl : forall x : E, R x x.
Axiom trans : forall x y z : E, R x y -> R y z -> R x z.
Axiom antisym : forall x y : E, R x y -> R y x -> x = y.

Definition smallest (x0 : E) := forall x : E, R x0 x.
Definition minimal (x0 : E) := forall x : E, R x x0 -> x = x0.

(* Exercice 1 *)
Lemma unicity_of_smallest :
  forall x : E, smallest x -> forall y : E, smallest y -> y = x.
Proof.
  intros x0 H1 y0 H2.
  unfold smallest in *. apply antisym.
  * apply H2.
  * apply H1.
Qed.

Lemma smallest_imply_minimal :
  forall x : E, smallest x -> minimal x.
Proof.
  intros x0 H1.
  unfold smallest in H1. unfold minimal.
  intros x1 H2.
  apply antisym.
  * assumption.
  * apply H1.
Qed.

Lemma unicity_of_minimal :
  forall x : E, smallest x -> forall y : E, minimal y -> x = y.
Proof.
  intros x H1 y H2.
  unfold smallest,minimal in *. apply H2. apply H1.
Qed.

(* Exercice 2 *)
Axiom not_not_elim : forall A : Prop, ~~A -> A.

Lemma excluded_middle : forall A : Prop, (A \/ ~A).
Proof.
  intro HA. apply not_not_elim.
  intro H. unfold not in H. apply H. right. intro A.
  apply H. left. assumption.
Qed.

Lemma paradoxe_du_buveur (P:E -> Prop) : exists x, P x -> forall y, P y.
Proof.
Admitted.

(* Exercice 3 *)
Definition subset (A : E->Prop) (B : E->Prop) := forall x, A x -> B x.

Parameter A B C : E -> Prop.

Lemma subset_refl : subset A A.
Proof.
  unfold subset. intros x HAx. assumption.
Qed.

Lemma subset_trans : subset A B -> subset B C -> subset A C.
Proof.
  intros HAB HBC. unfold subset in *.
  intro x. intro HAx. apply HBC. apply HAB. assumption.
Qed.

Definition eq (A : E -> Prop) (B : E -> Prop) := forall x, A x <-> B x.

Lemma subset_antisym : subset A B -> subset B A -> eq A B.
Proof.
  intros HAB HBA. unfold eq,subset in *. intro x. split.
  - intro HAx. apply HAB. assumption.
  - intro HBx. apply HBA. assumption.
Qed.

Definition union (A: E -> Prop) (B: E -> Prop) := fun x => A x \/ B x.
Definition inter (A: E -> Prop) (B: E -> Prop) := fun x => A x /\ B x.

Lemma union_assoc : eq (union A (union B C)) (union (union A B) C).
Proof.
Admitted.