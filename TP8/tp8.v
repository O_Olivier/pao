Parameter T: Type.

Inductive clos1 (R : T -> T -> Prop) : T -> T -> Prop :=
| cl1_base : forall x y, R x y -> clos1 R x y
| cl1_refl : forall x, clos1 R x x
| cl1_trans : forall x y z, clos1 R x y -> clos1 R y z -> clos1 R x z.

Check clos1_ind.

Lemma R_sym_clos1_sym : forall R : T -> T -> Prop, (forall x y : T, R x y -> R y x) -> (forall x y : T, (clos1 R) x y -> (clos1 R) y x).
Proof.
  intros R H_Rsym. induction 1 as [x y H | x H | x y z H1 IH1 H2 IH2].
  - apply cl1_base. apply H_Rsym. assumption.
  - apply cl1_refl.
  - apply cl1_trans with y.
    + assumption.
    + assumption.
Qed.

Lemma clos1_idempotent : forall R : T -> T -> Prop, forall x y : T, clos1 (clos1 R) x y -> clos1 R x y.
Proof.
  intros R x y H. induction H as [x y H | x H | x y z H1 IH1 H2 IH2].
  - assumption.
  - apply cl1_refl.
  - apply cl1_trans with (y:=y).
    + assumption.
    + assumption.
Qed.

Inductive clos2 (R : T -> T -> Prop) : T -> T -> Prop :=
| cl2_refl : forall x, clos2 R x x
| cl2_next : forall x y z, clos2 R x y -> R y z -> clos2 R x z.

Check clos2_ind.

Lemma clos2_clos1 : forall x y : T, forall R : T -> T -> Prop, clos2 R x y -> clos1 R x y.
Proof.
  intros x y R. induction 1 as [x | x y z H1 H2 H3].
  - apply cl1_refl.
  - apply cl1_trans with (y:=y).
    + assumption.
    + apply cl1_base in H3. assumption.
Qed.

Lemma R_clos2 : forall x y : T, forall R : T -> T -> Prop, R x y -> clos2 R x y.
  intros x y R H. apply cl2_next with (y:=x).
  - apply cl2_refl.
  - assumption.
Qed.

Parameter R : T -> T -> Prop.

Lemma clos2_trans : forall x y z : T, clos2 R x y -> clos2 R y z -> clos2 R x z.
Proof.
  intros x y z Hxy Hyz. 