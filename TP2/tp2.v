Check O.
Check S.
Check nat.
Print nat.
Search nat.
Check 2 + 2 = 5.
Definition id := fun (A : Set) (x : A) => x.
Check id nat 7.

Lemma and_commut :
  forall A B : Prop, A /\ B <-> B /\ A.
Proof.
  intros. split.
  - intros. destruct H. split. assumption. assumption.
  - intros. destruct H. split ; assumption.
Qed.


(* Exercice 1 *)
Parameters A B C : Prop.

Lemma E1F1 : A -> A.
Proof.
  intros. assumption.
Qed.

Lemma E1F2 : (A -> B) -> (B -> C) -> A -> C.
Proof.
  intros H0 H1 H2. apply H1. apply H0. apply H2.
Qed.

Lemma E1F3 : A /\ B <-> B /\ A.
Proof.
  split ; intro H ; destruct H as (H1,H2) ; split ; assumption.
Qed.

Lemma E1F4 : A \/ B <-> B \/ A.
Proof.
  split.
  - intro H. destruct H as [H1 | H2].
    * right. assumption.
    * left. assumption.
  - intro H. destruct H as [H1 | H2].
    * right. assumption.
    * left. assumption.
Qed.

Lemma E1F5 : (A /\ B) /\ C <-> A /\ (B /\ C).
Proof.
  split.
  - intro H. destruct H as (H1,H2). destruct H1. split.
    * assumption.
    * split; assumption.
  - intro H. destruct H as (H1, H2). destruct H2 as (H21, H22). split.
    * split; assumption.
    * assumption.
Qed.

Lemma E1F6 : (A \/ B) \/ C <-> A \/ (B \/ C).
Proof.
  split.
  - intro H. destruct H as [HAB | HC].
    * destruct HAB as [HA | HB].
      + left. assumption.
      + right. left. assumption.
    * right. right. assumption.
  - intro H. destruct H as [HA | HBC].
    * left. left. assumption.
    * destruct HBC.
      + left. right. assumption.
      + right. assumption.
Qed.

Lemma E1F7 : A -> ~~A.
Proof.
  intro H. intro HF. destruct HF. assumption.
Qed.

Lemma E1F8 : (A -> B) -> ~B -> ~A.
Proof.
  intro H. intro HNB. intro HA. destruct HNB. apply H. assumption.
Qed.

Lemma E1F9 : ~~(A \/ ~A).
Proof.
  intro H. unfold "~" in *. apply H. right. intro HA. apply H. left. assumption.
Qed.

(* Exercice 2 *)
Parameter X Y : Set.
Parameter P Q : X -> Prop.
Parameter R : X -> Y -> Prop.

Lemma E2F1 : (forall x, P x /\ Q x) <-> (forall x, P x) /\ (forall x, Q x).
Proof.
  split; intro H.
  + split; intro x; apply H.
  + intro x. split; elim H; intros HP HQ.
    * apply HP.
    * apply HQ.
Qed.

Lemma E2F2 : (exists x, P x \/ Q x) <-> (exists x, P x) \/ (exists x, Q x).
Proof.
  split; intro H.
  + destruct H as [x H]. elim H.
    * intro HP. left. exists x. assumption.
    * intro HQ. right. exists x. assumption.
  + destruct H ; destruct H as [x H].
    * exists x. left. assumption.
    * exists x. right. assumption.
Qed.

Lemma E2F3 : (exists y, forall x, R x y) -> forall x, exists y, R x y.
Proof.
  intro H. destruct H as [y H].
  intro x. exists y. apply H.
Qed.

(* Exercice 3 *)
Axiom not_not_elim : forall A : Prop, ~~A -> A.

Lemma Curry_isom : (A /\ B -> C) <-> (A -> B -> C).
Proof.
  split ; intro H.
  - intros HA HB. apply H. split.
    * assumption.
    * assumption.
  - intro HAB. destruct HAB as (HA,HB). apply H.
    * assumption.
    * assumption.
Qed.

Lemma E3F1 : (A /\ True) <-> A.
Proof.
  split ; intro H.
  - destruct H as (HA,_). assumption.
  - split.
    * assumption.
    * split.
Qed.

Lemma E3F2 : (A /\ False) <-> False.
Proof.
  split ; intro H.
  - destruct H as (_,HF). assumption.
  - destruct H.
Qed.

Lemma E3F3 : (A \/ False) <-> A.
Proof.
  split ; intro H.
  - destruct H as [HA | HF].
    * assumption.
    * destruct HF.
  - left. assumption.
Qed.

Lemma E3F4 : (A \/ True) <-> True.
Proof.
  split ; intro H.
  - split.
  - right. split.
Qed.

Lemma E3F5 : (A /\ (B \/ C)) <-> ((A /\ B) \/ (A /\ C)).
Proof.
  split ; intro H.
  - destruct H as (HA,HBC). destruct HBC as [HB | HC].
    + left. split ; assumption.
    + right. split ; assumption.
  - destruct H as [HAB | HAC].
    + destruct HAB as (HA,HB). split.
      * assumption.
      * left. assumption.
    + destruct HAC as (HA,HC). split.
      * assumption.
      * right. assumption.
Qed.

Lemma E3F6 : (A \/ (B /\ C)) <-> ((A \/ B) /\ (A \/ C)).
Proof.
  split ; intro H.
  - destruct H as [HA | HBC].
    + split ; left ; assumption.
    + destruct HBC as (HB,HC). split ; right ; assumption.
  - destruct H as (HAB,HAC). destruct HAB as [HA | HB].
    + left. assumption.
    + destruct HAC as [HA | HC].
      * left. assumption.
      * right. split ; assumption.
Qed.

Lemma E3F7 : (A -> (B /\ C)) <-> ((A -> B) /\ (A -> C)).
Proof.
  split ; intro H.
  - split ; intro HA. all: destruct H as [HB HC]. all:assumption.
  - intro HA. split ; destruct H as (H1,H2).
    + apply H1. assumption.
    + apply H2. assumption.
Qed.

Lemma E3F8 : ((A \/ B) -> C) <-> ((A -> C) /\ (B -> C)).
Proof.
  split ; intro H.
  - split ; intro HA.
    + apply H. left. assumption.
    + apply H. right. assumption.
  - intro HAB. destruct H as (HAC,HBC). destruct HAB as [HA | HB].
    + apply HAC. assumption.
    + apply HBC. assumption.
Qed.

Lemma E3F9 : (forall x, forall y, R x y) <-> (forall y, forall x, R y x).
Proof.
  split ; intro H.
  - intros y x. apply H.
  - intros x y. apply H.
Qed.

Lemma E3F10 : (exists x, exists y, R x y) <-> (exists y, exists x, R y x).
Proof.
  split ; intro H.
  - destruct H as (x0,H0). destruct H0 as (y0,H1).
    exists x0. exists y0. assumption.
  - destruct H as (y0,H0). destruct H0 as (x0,H1).
    exists y0. exists x0. assumption.
Qed.

Lemma double_neg : A <-> ~~A.
Proof.
  split ; intro H.
  - unfold not. intro HAF. apply HAF. assumption.
  - apply not_not_elim. assumption.
Qed.

Lemma excluded_middle : A \/ ~A.
Proof.
  apply not_not_elim.
  intro H. unfold not in *. apply H. right. intro HA. apply H. left. assumption.
Qed.

Lemma not_true_is_false : ~ True <-> False.
Proof.
  split ; intro H.
  - apply H. split.
  - destruct H.
Qed.

Lemma not_false_is_true : ~ False <-> True.
Proof.
  split ; intro H.
  - split.
  - intro HF. destruct HF.
Qed.

Lemma DeMorgan1 : ~ (A /\ B) <-> (~A) \/ (~B).
Proof.
  split ; intro H.
  - unfold not in *. Admitted.

Lemma DeMorgan2 : ~ (A \/ B) <-> (~A) /\ (~B).
Proof.
  split ; intro H.
  - split.
    + intro HA. apply H. left. assumption.
    + intro HB. apply H. right. assumption.
  - intro HAB. destruct H as (HNA,HNB). destruct HAB as [HA | HB].
    + apply HNA. assumption.
    + apply HNB. assumption.
Qed.

Lemma E3F11 : ~ (forall x, P x) <-> exists x, ~(P x).
Proof.
  split ; intro H.
  - Admitted.

Lemma E3F12 : ~ (exists x, P x) <-> forall x, ~(P x).
Admitted.

Lemma E3F13 : (A -> B) <-> (~A) \/ B.
Proof.
Admitted.

Lemma E3F14 : ~(A -> B) <-> A /\ ~B.
Proof.
Admitted.

Lemma Pierce_law : ((A -> B) -> A) -> A.
Proof.
Admitted.
