Require Import List.
Import ListNotations.

Inductive alpha := M | I | U.

Definition word := list alpha.

Inductive lang : word -> Prop :=
| axiom : lang [M;I]
| rule1 x : lang (x ++ [I]) -> lang (x ++ [I;U])
| rule2 x : lang ([M] ++ x) -> lang ([M] ++ x ++ x)
| rule3 x y : lang (x ++ [I;I;I] ++ y) -> lang (x ++ [U] ++ y)
| rule4 x y : lang (x ++ [U;U] ++ y) -> lang (x ++ y).

Lemma begin_with_M : forall w : word, lang w -> exists x : word, w = [M] ++ x.
Proof.
  induction 1 as [ | x H IH | x H IH | x y H IH | x y H IH ].
  - exists [I]. reflexivity.
  - elim IH. intros x0 H2. exists (x0 ++ [U]).
    rewrite app_assoc. rewrite <- H2. rewrite <- app_assoc. reflexivity.
  - exists (x ++ x). reflexivity.
  - elim IH. intros x0 H2. destruct x as [ | x xs ].
    + simpl in H2. discriminate H2.
    + exists (xs ++ [U] ++ y). rewrite <- app_comm_cons. simpl. f_equal.
      rewrite <- app_comm_cons in H2.
Admitted.


Definition first_letter (w : word) : option alpha :=
  match w with
  | []     => None
  | a :: _ => Some a
  end.

Lemma first_letter_is_M : forall w : word, lang w -> first_letter w = Some M.
Proof.
Admitted.


Inductive Z3 := Z0 | Z1 | Z2.

Definition succ (x : Z3) : Z3 :=
  match x with
  | Z0 => Z1
  | Z1 => Z2
  | Z2 => Z0
  end.

Definition add (x : Z3) (y : Z3) : Z3 :=
  match (x,y) with
  | (Z0,z) | (z,Z0) => z
  | (Z1,z) | (z,Z1) => succ z
  | (Z2,_) => succ (succ y)
  end.

Infix "+" := add.

Lemma add_commm : forall x y : Z3, x + y = y + x.
Proof.
  destruct x; intro y; destruct y; reflexivity.
Qed.

Lemma add_assoc : forall x y z : Z3, x + (y + z) = (x + y) + z.
Proof.
  destruct x; intros y z; destruct y; destruct z; reflexivity.
Qed.

Lemma Z0_identity : forall x : Z3, Z0 + x = x.
Proof.
  destruct x; reflexivity.
Qed.

Lemma sum_diff_Z0 : forall z : Z3, z <> Z0 -> z + z <> Z0.
Proof.
  intros z H. destruct z; unfold "+".
  - assumption.
  - unfold succ. discriminate.
  - unfold succ. discriminate.
Qed.

