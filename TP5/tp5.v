(* Exercice 1 *)

Lemma add_0_l : forall n, 0 + n = n.
Proof.
  intro n. simpl. reflexivity.
Qed.

Lemma add_succ_l : forall n m, S n + m = S (n + m).
Proof.
  intros n m. simpl. reflexivity.
Qed.

Lemma add_0_r : forall n, n + 0 = n.
Proof.
  induction n as [|n IHn].
  - simpl. reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Lemma add_succ_r : forall n m, n + S m = S (n + m).
Proof.
  induction n as [|n IHn] ; intro m.
  - simpl. reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Lemma add_assoc : forall n m p, (n + m) + p = n + (m + p).
Proof.
  induction n as [|n IHn] ; intros m p.
  - simpl. reflexivity.
  - simpl. rewrite IHn. reflexivity.
Qed.

Lemma add_comm : forall n m, n + m = m + n.
Proof.
  induction n as [|n IHn] ; intro m.
  - simpl. symmetry. apply add_0_r.
  - simpl. rewrite IHn. rewrite add_succ_r. reflexivity.
Qed.

(* Exercice 2 *)
Lemma mul_0_l : forall n, 0 * n = 0.
Proof.
  intro n. simpl. reflexivity.
Qed.

Lemma mul_succ_l : forall n m, S n * m = m + n * m.
Proof.
  intros n m. simpl. reflexivity.
Qed.

Lemma mul_0_r : forall n, n * 0 = 0.
Proof.
  induction n as [|n IHn] ; simpl.
  - reflexivity.
  - apply IHn.
Qed.

Lemma mul_succ_r : forall n m, n * S m = n + n * m.
Proof.
  induction n as [|n IHn] ; intro m ; simpl.
  - reflexivity.
  - f_equal. rewrite IHn. rewrite <- 2 add_assoc. f_equal. apply add_comm.
Qed.

Lemma mul_distr : forall n m p, (n + m) * p = n * p + m * p.
Proof.
  induction n as [|n IHn] ; intros m p ; simpl.
  - reflexivity.
  - rewrite IHn. rewrite add_assoc. reflexivity.
Qed.

Lemma mul_assoc : forall n m p, (n * m) * p = n * (m * p).
Proof.
  induction n as [|n IHn] ; intros m p ; simpl.
  - reflexivity.
  - rewrite <- IHn. apply mul_distr.
Qed.

Lemma mul_comm : forall n m, n * m = m * n.
Proof.
  induction n as [|n IHn] ; intro m ; simpl.
  - symmetry. apply mul_0_r.
  - rewrite IHn. rewrite mul_succ_r. reflexivity.
Qed.

(* Exercice 3 *)
Definition le (n m : nat) := exists p, n + p = m.
Infix "<=" := le.

Lemma le_refl : forall n, n <= n.
Proof.
  induction n as [|n IHn] ; unfold le ; simpl.
  - exists 0. reflexivity.
  - exists 0. rewrite add_0_r. reflexivity.
Qed.

Lemma le_trans : forall n m p, n <= m -> m <= p -> n <= p.
Proof.
  unfold le. intros n m p H0 H1.
  destruct H0 as (p0,H0). destruct H1 as (p1,H1).
  rewrite <- H0 in H1. exists (p0 + p1). rewrite <- add_assoc. assumption.
Qed.

Lemma le_antisym : forall n m, n <= m -> m <= n -> n = m.
Proof.
  