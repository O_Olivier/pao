Require Import List.
Import ListNotations.
Check list_ind.

(* Concatenation de listes *)
Check app.
Lemma nil_left { A : Type } : forall l : list A, nil ++ l = l.
Proof.
  intro l. simpl. reflexivity.
Qed.

Lemma nil_right { A : Type } : forall l : list A, l ++ nil = l.
Proof.
  intro l.
  induction l as [ | x xs IHl].
  - reflexivity.
  - simpl. rewrite IHl. reflexivity.
Qed.

Lemma app_assoc { A : Type } : forall l1 l2 l3 : list A, (l1 ++ l2) ++ l3 = l1 ++ (l2 ++ l3).
Proof.
  intros l1 l2 l3.
  induction l1 as [ | x xs IHl1].
  - reflexivity.
  - simpl. rewrite IHl1. reflexivity.
Qed.

(* Longueur *)
Check length.

Fixpoint length {A:Type} (l : list A) : nat :=
  match l with
  | [] => O
  | _::xs => S (length xs)
  end.

Check @length.

Lemma length_lemma {A:Type} : forall l1 l2 : list A, length (l1 ++ l2) = length l1 + length l2.
Proof.
  intros l1 l2.
  induction l1 as [ | x xs IHl1].
  - reflexivity.
  - simpl. rewrite IHl1. reflexivity.
Qed.


(* Retournement *)
Fixpoint rev_append {A:Type} (l1 l2 : list A) : list A :=
  match l1 with
  | [] => l2
  | x::xs => rev_append xs (x::l2)
  end.

Definition rev {A:Type} (l : list A) : list A := rev_append l [].

Require Import Arith.

Lemma rev_append_lemma {A:Type} : forall l1 l2 : list A, length (rev_append l1 l2) = length l1 + length l2.
Proof.
  induction l1 as [ | x xs IHl1].
  - reflexivity.
  - intro l2. simpl. rewrite IHl1. simpl. apply Nat.add_succ_r.
Qed.

Lemma rev_lemma {A:Type} : forall l : list A, length (rev l) = length l.
Proof.
  induction l as [ | x xs IHl].
  - reflexivity.
  - unfold rev. simpl. rewrite rev_append_lemma. simpl. rewrite Nat.add_1_r. reflexivity.
Qed.


Lemma rev_append_lemma2 {A:Type} : forall l1 l2 l3 : list A, rev_append l1 (l3 ++ l2) = (rev_append l1 l3) ++ l2.
Proof.
  induction l1 as [ | x xs IHl1].
  - reflexivity.
  - simpl. intros l2 l3. rewrite <- IHl1. simpl. reflexivity.
Qed.

Lemma rev_append_lemma3 {A:Type} : forall l1 l2 : list A, rev_append l1 l2 = (rev_append l1 []) ++ l2.
Proof.
  intros l1 l2. rewrite <- rev_append_lemma2. reflexivity.
Qed.

(* rev (l1++l2) = rev_append l2 (rev l1) = rev l2 ++ rev l1 *)

Lemma rev_lemma2 {A:Type} : forall l1 l2 : list A, rev (l1 ++ l2) = (rev l2) ++ (rev l1).
Proof.
  induction l1 as [ | x xs IHl].
  - unfold rev. simpl. intro l2. rewrite <- rev_append_lemma3. reflexivity.
  - intro l2. unfold rev in *. simpl.
Admitted.